mw.loader.load('http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML&delayStartupUntil=configured');mw.loader.load('http://cyclowiki.org/w/index.php?title=MediaWiki:MathJax.js&action=raw&ctype=text/javascript&smaxage=21600&maxage=86400');importScriptURI('http://wikireality.ru/w/index.php?title=MediaWiki:Editpage.js&action=raw&ctype=text/javascript');addLoadEvent=addOnloadHook
importScript_=importScript
var addLoadEvent=addOnloadHook;var import_script=importScript;var import_style=importStylesheet;var importScript_=importScript;function addLoadEvent(func){if(window.addEventListener)window.addEventListener("load",func,false);else if(window.attachEvent)window.attachEvent("onload",func);}if(wgAction=='view'&&wgNamespaceNumber>=0)addOnloadHook(function(){var h2s=document.getElementsByTagName('H2');var h2=h2s[0];if(!h2)return;if(h2.parentNode.id=='toctitle')h2=h2s[1];if(!h2)return;var span=h2.firstChild;if(!span||span.className!='editsection')return;var zero=span.cloneNode(true);if(document.getElementById('featured-star'))zero.style.marginRight='25px'
var parent=document.getElementsByTagName('H1')[0];parent.insertBefore(zero,parent.firstChild);var a=zero.getElementsByTagName('A')[0];a.title=a.title.replace(/:.*$/,' после заголовка');a.setAttribute('href',a.href.replace(/&section=1/,'&section=0'));})
var hasClass=(function(){var reCache={};return function(element,className){return(reCache[className]?reCache[className]:(reCache[className]=new RegExp("(?:\\s|^)"+className+"(?:\\s|$)"))).test(element.className);};})();var withJS=document.URL.match(/[&?]withjs=((mediawiki:)?([^&#]+))/i);if(withJS)importScript('MediaWiki:'+withJS[3]);var NavigationBarHide='[скрыть]';var NavigationBarShow='[показать]';var NavigationBarShowDefault=2;function collapsibleTables(){var Table,HRow,HCell,btn,a,tblIdx=0,colTables=[]
var allTables=document.getElementsByTagName('table')
for(var i=0;Table=allTables[i];i++){if(!hasClass(Table,'collapsible'))continue
if(!(HRow=Table.rows[0]))continue
if(!(HCell=HRow.getElementsByTagName('th')[0]))continue
Table.id='collapsibleTable'+tblIdx
btn=document.createElement('span')
btn.style.cssText='float:right; font-weight:normal; font-size:smaller'
a=document.createElement('a')
a.id='collapseButton'+tblIdx
a.href='javascript:collapseTable('+tblIdx+');'
a.style.color=HCell.style.color
a.appendChild(document.createTextNode(NavigationBarHide))
btn.appendChild(a)
HCell.insertBefore(btn,HCell.childNodes[0])
colTables[tblIdx++]=Table}for(var i=0;i<tblIdx;i++)if((tblIdx>NavigationBarShowDefault&&hasClass(colTables[i],'autocollapse'))||hasClass(colTables[i],'collapsed'))collapseTable(i)}function collapseTable(idx){var Table=document.getElementById('collapsibleTable'+idx)
var btn=document.getElementById('collapseButton'+idx)
if(!Table||!btn)return false
var Rows=Table.rows
var isShown=(btn.firstChild.data==NavigationBarHide)
btn.firstChild.data=isShown?NavigationBarShow:NavigationBarHide
var disp=isShown?'none':Rows[0].style.display
for(var i=1;i<Rows.length;i++)Rows[i].style.display=disp}function collapsibleDivs(){var navIdx=0,colNavs=[],i,NavFrame
var divs=document.getElementById('content').getElementsByTagName('div')
for(i=0;NavFrame=divs[i];i++){if(!hasClass(NavFrame,'NavFrame'))continue
NavFrame.id='NavFrame'+navIdx
var a=document.createElement('a')
a.className='NavToggle'
a.id='NavToggle'+navIdx
a.href='javascript:collapseDiv('+navIdx+');'
a.appendChild(document.createTextNode(NavigationBarHide))
for(var j=0;j<NavFrame.childNodes.length;j++)if(hasClass(NavFrame.childNodes[j],'NavHead'))NavFrame.childNodes[j].appendChild(a)
colNavs[navIdx++]=NavFrame}for(i=0;i<navIdx;i++)if((navIdx>NavigationBarShowDefault&&!hasClass(colNavs[i],'expanded'))||hasClass(colNavs[i],'collapsed'))collapseDiv(i)}function collapseDiv(idx){var div=document.getElementById('NavFrame'+idx)
var btn=document.getElementById('NavToggle'+idx)
if(!div||!btn)return false
var isShown=(btn.firstChild.data==NavigationBarHide)
btn.firstChild.data=isShown?NavigationBarShow:NavigationBarHide
var disp=isShown?'none':'block'
for(var child=div.firstChild;child!=null;child=child.nextSibling)if(hasClass(child,'NavPic')||hasClass(child,'NavContent'))child.style.display=disp}addOnloadHook(collapseDiv);addOnloadHook(collapseTable);addOnloadHook(collapsibleDivs);addOnloadHook(collapsibleTables);addOnloadHook(function(){$('.jnav').each(function(i,e){$(this).data('i',i+1).click(function(){var $this=$(this),$jnavb=$('#jnavb-'+$this.data('i'));if($this.hasClass('jnav-inactive')){$('.jnav-active').removeClass('jnav-active').addClass('jnav-inactive');$('.jnavb').hide(250);$this.removeClass('jnav-inactive').addClass('jnav-active');$jnavb.show(250);}else{$this.removeClass('jnav-active').addClass('jnav-inactive');$jnavb.hide(250);}return false;});});});$(function(){if(wgAction!='view'||!$('.coordinates').text())return;c=$('.coordinates')[0];$('.coordinates').remove();$('#siteSub').before(c);$('.coordinates').css('position','relative').css('right','0').css('top','0').css('margin-top','-7px').css('margin-left','2.8em').
addClass('plainlinks');});jQuery.fn.addtocopy=function(usercopytxt){var options={htmlcopytxt:'<br>More: <a href="'+window.location.href+'">'+window.location.href+'</a><br>',minlen:25,addcopyfirst:false}
$.extend(options,usercopytxt);var copy_sp=document.createElement('span');copy_sp.id='ctrlcopy';copy_sp.innerHTML=options.htmlcopytxt;return this.each(function(){$(this).mousedown(function(){$('#ctrlcopy').remove();});$(this).mouseup(function(){if(window.getSelection){var slcted=window.getSelection();var seltxt=slcted.toString();if(!seltxt||seltxt.length<options.minlen)return;var nslct=slcted.getRangeAt(0);seltxt=nslct.cloneRange();seltxt.collapse(options.addcopyfirst);seltxt.insertNode(copy_sp);if(!options.addcopyfirst)nslct.setEndAfter(copy_sp);slcted.removeAllRanges();slcted.addRange(nslct);}else if(document.selection){var slcted=document.selection;var nslct=slcted.createRange();var seltxt=nslct.text;if(!seltxt||seltxt.length<options.minlen)return;seltxt=nslct.duplicate();seltxt.collapse(options.addcopyfirst);seltxt.pasteHTML(copy_sp.outerHTML);if(!options.addcopyfirst){nslct.setEndPoint("EndToEnd",seltxt);nslct.select();}}});});}
if(wgUserName==null){$(document).addtocopy({htmlcopytxt:'<br>Подробнее: <a href="'+window.location.href+'">'+window.location.href+'</a>'});}$(function(){a=$('#wpTextbox1').val();if($.inArray("Wharticle",wgCategories)!=-1||$.inArray("Whcat",wgCategories)!=-1||$.inArray("Whtalk",wgCategories)!=-1||(a!=undefined&&(a.indexOf('{{wharticle}}')!=-1||a.indexOf('{{whcat}}')!=-1||a.indexOf('{{whtalk')!=-1))){$('head').append('<link rel="stylesheet" href="/w/index.php?title=MediaWiki:Wh.css&action=raw&ctype=text/css" type="text/css" />');}});;mw.loader.state({"site":"ready"});
/* cache key: cyclo_pub:resourceloader:filter:minify-js:7:627f610515c3624190d2378fe1e5ce79 */