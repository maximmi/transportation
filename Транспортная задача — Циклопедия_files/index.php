MathJax.Hub.Config ({
    tex2jax: {
        inlineMath: [["\x3Cmath\x3E", "\x3C/math\x3E"]],
        displayMath: [],
        element: "bodyContent",
        processEnvironments: false,
        ignoreClass: "diff",
        skipTags: ["script", "noscript", "style", "textarea", "pre", "code", "tt", "nowiki"],
    },
    messageStyle: "none",
    showProcessingMessages: false,
});

MathJax.Hub.Configured();