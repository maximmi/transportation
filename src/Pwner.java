import java.util.ArrayList;
import java.util.LinkedList;

class Cell {
	protected Integer val,i,j;
	public Cell(Integer val, Integer i, Integer j) {
		super();
		this.val = val;
		this.i = i;
		this.j = j;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((i == null) ? 0 : i.hashCode());
		result = prime * result + ((j == null) ? 0 : j.hashCode());
		result = prime * result + ((val == null) ? 0 : val.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cell other = (Cell) obj;
		if (i == null) {
			if (other.i != null)
				return false;
		} else if (!i.equals(other.i))
			return false;
		if (j == null) {
			if (other.j != null)
				return false;
		} else if (!j.equals(other.j))
			return false;
		if (val == null) {
			if (other.val != null)
				return false;
		} else if (!val.equals(other.val))
			return false;
		return true;
	}
	
	
}

public class Pwner {
	private Cell[][] sol;
	private Integer[][] cs;
	private ArrayList<String> __out = new ArrayList<>();
	private Integer[] needs, sups,u,v;
	private Integer rown,coln,ist,jst;
	//ArrayList<Integer> icycle,jcycle;
	private Cell[] cycle;
	private Integer lasti;
	private Integer lastj;
	
	public Pwner(Integer[][] cs)
	{
		this.cs = cs;
		this.rown = cs.length;
		this.coln = cs[0].length;
		sol = new Cell[rown][coln];
		for (int i=1; i<rown; i++ )
			for (int j=1; j<coln; j++)
			{
				sol[i][j] = new Cell(null, i,j);
			}
		//needs = cs[0]; ������� ��� �� �������!!! ��� ���������� �� ���������!
		needs = new Integer[coln];
		sups = new Integer[rown];
		u = new Integer[rown];
		v = new Integer[coln];
		for (int i = 1; i<rown; i++)
		{
			sups[i] = cs[i][0];
		}
		for (int i = 1; i<coln; i++)
		{
			needs[i] = cs[0][i];
		}
		//icycle = new ArrayList<Integer>();
		//jcycle = new ArrayList<Integer>();
	}
	
	public void pwn()
	{
	//	Integer x = Integer.MAX_VALUE;
		genFirstSol();
		int basecells=0;
		for (int i = 1; i<rown; i++)
		{
			for (int j = 1; j<coln; j++)
			{
				if (sol[i][j].val!=null) basecells++; 
			}
		}
		if (basecells<rown+coln-3) {fix(); while(!genPotentials()) fix();}
		else genPotentials();
		int fuse=100;
		boolean opt = false;
		while (!opt && fuse>=0)
		{
			try
			{
				opt = checkOpt();
			}
			catch (Exception e)
			{
				e.printStackTrace();
				break;
			}
			fuse--;
			//x=price();
			clearAfterFix();
			cycle = findCycle(sol[ist][jst]);
			printCycle();
			try {
				pwnCycle();
			} catch (Exception e) {
				System.err.println(e.getMessage());
				e.printStackTrace();
			}
			basecells=0;
			for (int i = 1; i<rown; i++)
			{
				for (int j = 1; j<coln; j++)
				{
					if (sol[i][j].val!=null) basecells++; 
				}
			}
			if (basecells<rown+coln-3) {fix(); while(!genPotentials()) fix();}
			else genPotentials();
		}
	}
	
	public Integer[][] out()
	{
		try
		{
		Integer[][] tmp = new Integer[rown][coln];
		for (int i=1; i<rown; i++ )
			for (int j=1; j<coln; j++)
			{
				if (sol[i][j]==null) tmp[i][j]=0;
				else tmp[i][j]=(sol[i][j].val);
			}
		return tmp;
		}
		catch (NullPointerException e)
		{
			return null;
		}
	}
	
	public Object[] getLog()
	{
		return __out.toArray();
	}
	
	public void genFirstSol()
	{
		Integer[] need = needs, supp = sups;
		for (int i = 1; i<rown; i++)
		{
			for (int j = 1; j<coln; j++)
			{
				if (supp[i]==0) break;
				
				int part = need[j];
				if (part==0) continue;
				
				if (supp[i]<=part) part = supp[i];
				
				need[j]-=part;
				supp[i]-=part;
				sol[i][j].val=part;
			}
		}
	}
	
	protected void print(Object obj)
	{
		System.out.print(obj);
		__out.add(obj.toString());		
	}
	
	protected void println()
	{
		System.out.println();
		__out.add("\n");
	}
	
	protected void printArray(Integer[] a)
	{
		for (int i=0; i<a.length; i++)
		{
			print(a[i]+" ");
		}
		println();
	}
	
	protected void printCycle()
	{
		for (int i=0; i<cycle.length;i++)
		{
			print("("+cycle[i].i+";"+cycle[i].j+"); ");
		}
		println();
	}
	
	int fuse;
		
	public boolean genPotentials()
	{
		u = new Integer[rown];
		v = new Integer[coln];
		u[1]=0;
		boolean fuckup=false;
		fuse = rown*coln;
		try {
			genU(1);
		} catch (Exception e) {
			System.err.println(e.getMessage());
			fuckup=true;
		}
		printArray(u);
		printArray(v);
		return !fuckup;
	}
	
	
	public boolean fix() //sometimes fail
	{
		int i,j;
		boolean ok=false;
		if (lasti!=null && lastj!=null) sol[lasti][lastj].val = null; //here
		for (i = 1; i<rown; i++)
		{
			for (j = 1; j<coln; j++)
			{
				if (sol[i][j].val==null) 
					{
					ok=true;
					break;
					}
			}
			if (ok) break;
		}
		if (ok) {print("fix called"); println();}
		while(ok)
		{
			i=(int) Math.ceil(Math.random()*rown-1);
			j=(int) Math.ceil(Math.random()*coln-1);
			if (sol[i][j]==null || sol[i][j].val!=null) continue;
			if (findCycle(sol[i][j]).length==0)
			{
				sol[i][j].val=0;
				lasti=i; lastj=j;
				return true;
			} 
		}
		return false;
	}
	
	protected void clearAfterFix()
	{
		lasti=null;
		lastj=null;
		for (int i = 1; i<rown; i++)
		{
			for (int j = 1; j<coln; j++)
			{
				sol[i][j].i=i;
				sol[i][j].j=j;
			}
		}
	}
	
	protected void genV(int j) throws Exception
	{
		if (v[j]==null) throw new Exception ("null found at genV");
		
		for (int i=1; i<rown; i++)
		{
			if (sol[i][j].val == null) continue;
			if (u[i]!=null) continue;
				else 
				{
					u[i]=cs[i][j]-v[j];
					genU(i);
				}		
		}
	}
	
	protected void genU(int i) throws Exception
	{
		fuse--;
		if (fuse<=0) throw new Exception ("Infinite loop detected at genU"); 
		if (u[i]==null) throw new Exception ("null found at genU");
		for (int j=1; j<coln; j++)
		{
			if (sol[i][j].val == null) continue;
			if (v[j]!=null) continue;
				else 
				{
					v[j]=cs[i][j]-u[i];
					genV(j);
				}					
		}
	}
	
	protected boolean checkOpt() throws NullPointerException
	{
		boolean isOpt=true;
		int dmin=0,i,j,d;
		for (i = 1; i<rown; i++)
		{
			for (j = 1; j<coln; j++)
			{
				if (sol[i][j].val!=null) d=0;
				else d = cs[i][j]-u[i]-v[j];
				
				if (d<0) isOpt=false;
				if (d<dmin)
				{
					dmin=d;
					ist=i;
					jst=j;
				}
			}
		}
		print(ist+" "+jst+" "+isOpt);
		println();
		return isOpt;
	}
	
	public int price()
	{
		int sum=0,i,j;
		for (i = 1; i<rown; i++)
		{
			for (j = 1; j<coln; j++)
			{
				if (sol[i][j].val !=null)
					sum+=sol[i][j].val*cs[i][j];
			}
		}
		print(sum);
		println();
		return sum;
	}
	
	public LinkedList<Cell> arrToList(Cell[][] in)
	{
		LinkedList<Cell> tmp = new LinkedList<>();
		for (int i=1; i<in.length; i++ )
			for (int j=1; j<in[i].length; j++)
			{
				tmp.add(in[i][j]);
			}
		return tmp;
	}
	
	protected Cell[] findCycle(Cell s) {
	        LinkedList<Cell> cycle = arrToList(sol);
	     //   cycle.addFirst(s);
	 
	        while (cycle.removeIf(thisCell -> {
	            Cell[] nrs = getNearest(thisCell, cycle);
	            return nrs[0] == null || nrs[1] == null;
	        }));
	        
	        for (Cell cell : cycle)
	        {
	        	print("("+cell.i+";"+cell.j+"); ");
	        }
	        println();
	        
	        Cell[] members = cycle.toArray(new Cell[cycle.size()]);
	        Cell prev = s;
	        
	        for (int i = 0; i < members.length; i++) {
	        	members[i] = prev;
	            prev = getNearest(prev, cycle)[i % 2];
	        }
	        return members;
	    }
	
	//(c) Maxim Metel, IVT-142. If you see this, please, send this student to far journey.
	 
	protected Cell[] getNearest(Cell centerCell, LinkedList<Cell> lst) {
	    	Cell[] nrs = new Cell[2];

//	    	print(centerCell.i+";"+centerCell.j+";");
	        for (Cell cell : lst) {
	            if ((cell.val != null && (centerCell.val != null || centerCell.equals(sol[ist][jst]))  && !cell.equals(centerCell)) || (cell.equals(sol[ist][jst]) && !(centerCell.equals(sol[ist][jst])))) {                
	            	if (cell.i == centerCell.i && nrs[0] == null)
	                    nrs[0] = cell;
	                else if (cell.j == centerCell.j && nrs[1] == null)
	                    nrs[1] = cell;
	                if (nrs[0] != null && nrs[1] != null)
	                {
//	                	print("nrs: ");
	                    break;
	                }
	            }
	        }
/*        	if(nrs[0]!=null) print(nrs[0].i+" "+nrs[0].j+"...");
        	print(";");
        	if(nrs[1]!=null) print(nrs[1].i+" "+nrs[1].j);
        	println();*/
	        return nrs;
	    }
	
	public boolean pwnCycle() throws Exception
	{
		//if (jcycle.size()!=icycle.size()) throw new Exception("Non-square cycle");
		if (cycle.length<4) throw new Exception("Too small cycle");
		int t=Integer.MAX_VALUE,pwn;
		boolean minus = false;
		for (int k = 0; k<cycle.length; k++)
		{
			int i=cycle[k].i;
			int j=cycle[k].j;
			if (minus) 
			{
				pwn=sol[i][j].val;
				if(pwn<t)
				{
					t=pwn;
				}
				minus=false;
			} else minus=true;
		}
		print(t);
		if (t==0) return false;
		minus=false;
		for (int k = 0; k<cycle.length; k++)
		{
			int i=cycle[k].i;
			int j=cycle[k].j;
			if (sol[i][j].val==null) sol[i][j].val=0; 
			if(minus) 
			{
				sol[i][j].val-=t;
				minus=false;
			}
			else
			{
				sol[i][j].val+=t;
				minus=true;
			}
			if (sol[i][j].val==0) sol[i][j].val=null; 
		}
		return true;
	}
}
