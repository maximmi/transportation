import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.LinkedList;
import java.awt.event.ActionEvent;
import javax.swing.JTextArea;
import java.awt.Font;

public class MainForm {

	private JFrame frame;
	private JTable table;
	private JTextField rows;
	private JTextField cols;
	private JTable otable;
	private JButton btnLaunch;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainForm window = new MainForm();
					window.frame.setVisible(true);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MainForm() {
		initialize();
	}
	
	private Integer[][] closeTable(Integer[][] cs)
	{
		Integer[][] tmp;
		int fictCol=0, fictRow=0;
		int rowSum=0;
			int colSum=0;
			
			for (int j=1; j<cs[0].length; j++)
			{
				colSum+=cs[0][j];
			}
			for (int i=1; i<cs.length; i++)
			{
				rowSum+=cs[i][0];
			}
			if (colSum>rowSum) fictRow = colSum-rowSum;
			if (rowSum>colSum) fictCol = rowSum-colSum;
			if (fictRow>0)
			{
				tmp = new Integer[cs.length+1][cs[0].length];
			}
			else if (fictCol>0)
			{
				tmp = new Integer[cs.length][cs[0].length+1];
			}
			else return cs;
			for (int i=0; i<cs.length; i++)
				for (int j=0; j<cs[0].length; j++)
				{
					tmp[i][j] = cs[i][j];
				}
			if (fictRow>0)
			{
				tmp[tmp.length-1][0] = fictRow;
				for (int j=1; j<tmp[0].length; j++)
				{
					tmp[tmp.length-1][j]=0;
				}
			}
			else if (fictCol>0)
			{
				tmp[0][tmp[0].length-1] = fictCol;
				for (int i=1; i<tmp.length; i++)
				{
					tmp[i][tmp[0].length-1]=0;
				}
			}
			return tmp;
		
	}
	
	private int f = 0;
	private Integer[][] cs;
	private Integer[][] cs2;
	private Integer minPrice=Integer.MAX_VALUE;
	private JTextArea textArea;
	private Integer[][] sol;

	private boolean fuck()
	{
		boolean good=false;
			Pwner pwner = new Pwner(cs2);
			try
			{
			pwner.pwn();
			good = pwner.checkOpt();
			if (good)
				{
				sol = pwner.out();
				minPrice = pwner.price();
				}
			return good;
			}
			catch (NullPointerException e)
			{
				System.err.println("Fucking null. Again. Restarting...");
				return false;
			}
	}
	
	
	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setResizable(false);
		frame.setBounds(100, 100, 640, 336);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		table = new JTable();
		table.setModel(new DefaultTableModel(
			new Object[][] {
//				{"0", "20", "30", "30", "10"}, 
//				{"30", "2", "3", "2", "4"},
//				{"40", "3", "2", "5", "1"},
//				{"20", "4", "3", "2", "6"},
//				{"0","100","40","80","60"},
//				{"160","4","8","10","5"},
//				{"30","4","6","2","3"},
//				{"90","4","4","6","5"},
//				{"0","150","130","150","140"},
//				{"200","7","8","1","2"},
//				{"180","4","5","9","8"},
//				{"190","9","2","3","6"},
				//(c) Maxim Metel, IVT-142. If you see this, 
				//please, send this student to far journey.
//				{"0","20","40","30","10","50","25"},
//				{"30","1","2","1","4","5","2"},
//				{"50","3","3","2","1","4","3"},
//				{"75","4","2","5","9","6","2"},
//				{"20","3","1","7","3","4","6"}
//				{"0","20","30","10"},
//				{"12","3","5","7"},
//				{"40","2","4","6"},
//				{"33","9","1","8"},
				{"0","10","15","12","15"},
				{"14","10","30","25","15"},
				{"10","20","15","20","10"},
				{"15","10","30","20","20"},
				{"12","30","40","35","45"},
			},
			new String[] {
				"New column", "New column", "New column", "New column","New column"//,"New column","New column"
			}
		));
		table.setBounds(10, 11, 403, 77);
		frame.getContentPane().add(table);
		
		otable = new JTable();

		otable.setBounds(10, 99, 403, 77);
		frame.getContentPane().add(otable);
		
		rows = new JTextField();
		rows.setBounds(426, 24, 86, 20);
		frame.getContentPane().add(rows);
		rows.setColumns(10);
		
		cols = new JTextField();
		cols.setBounds(20, 277, 86, 20);
		frame.getContentPane().add(cols);
		cols.setColumns(10);
		
		JLabel label = new JLabel("\u0421\u0442\u043E\u043B\u0431\u0446\u044B");
		label.setBounds(20, 263, 46, 14);
		frame.getContentPane().add(label);
		
		JLabel label_1 = new JLabel("\u0421\u0442\u0440\u043E\u043A\u0438");
		label_1.setBounds(423, 11, 46, 14);
		frame.getContentPane().add(label_1);
		
		JButton btnNewButton = new JButton("\u0421\u043E\u0437\u0434\u0430\u0442\u044C");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Integer rown = Integer.parseInt(rows.getText());
				Integer coln = Integer.parseInt(cols.getText());
				Object cells[][] = new Object[rown][coln];
				String colnames[] = new String[coln]; 
				table.setModel(new DefaultTableModel(cells,colnames));
				otable.setModel(new DefaultTableModel(cells,colnames));
//				for (int i=0; i<rown; i++)
//				{
//					for (int j=0; j<coln; j++)
//					{						
//						table.getModel().setValueAt(String.valueOf(i)+String.valueOf(j), i, j);
//					}
//					
//				}

				
			}
		});
		btnNewButton.setBounds(118, 272, 89, 23);
		frame.getContentPane().add(btnNewButton);
		
		JLabel total = new JLabel("Total: ");
		total.setBounds(319, 280, 46, 14);
		frame.getContentPane().add(total);
		
		btnLaunch = new JButton("\u041F\u0423\u0421\u041A");
		btnLaunch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				boolean good=false;
				cs = new Integer[table.getModel().getRowCount()][table.getModel().getColumnCount()];
				for (int i = 0; i<table.getModel().getRowCount(); i++)
					for (int j=0; j<table.getModel().getColumnCount(); j++)
					{
						cs[i][j] = Integer.parseInt((String)table.getModel().getValueAt(i, j));
					}
				cs2 = closeTable(cs);
				otable.setModel(new DefaultTableModel(
						new Object[cs2.length][cs2[0].length],
						new String[cs2[0].length]
					));
				do
					{
					good = fuck();
					}
				while (!good);
				
			for (int i = 0; i<sol.length; i++)
					for (int j=0; j<sol[0].length; j++)
					{
						otable.getModel().setValueAt(sol[i][j], i, j);
					}
			total.setText(minPrice.toString());
			}

		});
		btnLaunch.setBounds(220, 272, 89, 23);
		frame.getContentPane().add(btnLaunch);
		
		textArea = new JTextArea();
		textArea.setLineWrap(true);
		textArea.setFont(new Font("Monospaced", Font.PLAIN, 8));
		textArea.setEditable(false);
		textArea.setBounds(423, 55, 201, 242);
		frame.getContentPane().add(textArea);
		
	}
}
